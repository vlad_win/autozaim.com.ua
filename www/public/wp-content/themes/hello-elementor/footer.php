<?php
/**
 * The template for displaying the footer.
 *
 * Contains the body & html closing tags.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footer' ) ) {
	get_template_part( 'template-parts/footer' );
}
?>

<?php wp_footer(); ?>
<script type="text/javascript">
  (function(d, w, s) {
  var widgetHash = 'nakictyc5aj8h2h4f45p', gcw = d.createElement(s); gcw.type = 'text/javascript'; gcw.async = true;
  gcw.src = '//widgets.binotel.com/getcall/widgets/'+ widgetHash +'.js';
  var sn = d.getElementsByTagName(s)[0]; sn.parentNode.insertBefore(gcw, sn);
  })(document, window, 'script');
</script>

</body>
</html>
